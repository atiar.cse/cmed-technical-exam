<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescriptions', function (Blueprint $table) {        
            $table->increments('id');
            $table->date('prescription_date');
            $table->string('patient_name', 191);
            $table->integer('age')->length(3);
            $table->string('gender', 50);
            $table->text('diagnosis')->nullable();
            $table->text('medicines')->nullable();
            $table->date('next_visit_date')->nullable()->default(null);
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescriptions');
    }
}
