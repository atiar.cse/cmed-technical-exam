@extends('layouts.app')

@section('content')
<div class="container">
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-6">
            <h3>Prescriptions - Day Wise Count</h3>
        </div>
        <div class="col-md-6 text-right">
            <a class="btn btn-sm btn-primary pull-right" href="/prescriptions/create" role="button">+ Add New</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Day</th>
                        <th>Prescription Count</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($prescriptions as $prescription)
                        <tr>
                          <td>{{$prescription->prescription_date}}</td>
                          <td>{{$prescription->dailyCount}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>            
        </div>
    </div>
</div>

@endsection
