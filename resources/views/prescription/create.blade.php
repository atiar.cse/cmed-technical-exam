@extends('layouts.app')

@section('content')
<div class="container">
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-6">
            <h3>Create Prescription</h3>
        </div>
        <div class="col-md-6 text-right">

        </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-12">
        <form role="form" id="prescription_form" method="POST" action="/prescriptions">
         {{csrf_field()}}
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="prescription_date">Prescription Date *</label>
              <input type="date" class="form-control datepicker" name="prescription_date" id="prescription_date" placeholder="Prescription Date" value="{{date('Y-m-d')}}" required>
            </div>
            <div class="form-group col-md-6">
              <label for="patient_name">Patient Name *</label>
              <input type="text" class="form-control" name="patient_name" id="patient_name" placeholder="Patient Name" value="{{old('patient_name')}}" required>
            </div>
          </div>
          <div class="form-group">
            <label for="age">Patient Age *</label>
            <input type="number" min="1" max="120" class="form-control" name="age" id="age" placeholder="Age"  value="{{old('age')}}" required>
          </div>
          <div class="form-group">
            <div class="form-group">
              <label for="gender">Patient Gender *</label>
              <select name="gender" id="gender" class="form-control" required>
                <option value="" selected>Choose...</option>
                <option value="Male">Male</option> 
                <option value="Female">Female</option>
              </select>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="diagnosis">Diagnosis</label>
              <textarea class="form-control" name="diagnosis" id="diagnosis">{{old('diagnosis')}}</textarea>
            </div>
            <div class="form-group col-md-6">
              <label for="medicines">Medicines</label>
              <textarea class="form-control" name="medicines" id="medicines">{{old('medicines')}}</textarea>
            </div>
            <div class="form-group col-md-6">
              <label for="next_visit_date">Next visit date</label>
              <input type="date" class="form-control" name="next_visit_date" id="next_visit_date" placeholder="Date">
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Save</button>
        </form>        
      </div>
    </div>
</div>
@endsection
