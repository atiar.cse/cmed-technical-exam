@extends('layouts.app')

@section('content')
<div class="container">
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-6">
            <h3>Prescriptions</h3>
        </div>
        <div class="col-md-6 text-right">
            <a class="btn btn-sm btn-primary pull-right" href="/prescriptions/create" role="button">+ Add New</a>
        </div>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <div class="col-md-12">
          <form id="monthlyData" method="GET" action="/prescriptions">
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="start_date">Start Date</label>
                <input type="date" class="form-control" name="start_date" id="start_date" placeholder="Start Date" value="{{$start_date}}">
              </div>
              <div class="form-group col-md-4">
                <label for="end_date">End Date</label>
                <input type="date" class="form-control" name="end_date" id="end_date" placeholder="End Date" value="{{$end_date}}">
              </div>
              <div class="form-group col-md-4">
                <label for="">&nbsp;</label>
                <button type="submit" class="form-control btn btn-success">Search</button>
              </div>
            </div> 
          </form> 
        </div>       
      </div>
    </div>
    <div class="row">
        <div class="col-md-12">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Prescription Date</th>
                        <th>Patient Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Next visit date</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($prescriptions as $prescription)
                        <tr>
                          <td>{{$prescription->prescription_date}}</td>
                          <td>{{$prescription->patient_name}}</td>
                          <td>{{$prescription->age}}</td>
                          <td>{{$prescription->gender}}</td>
                          <td>{{$prescription->next_visit_date}}</td>
                          <td>
                            <a class="btn btn-sm btn-info pull-right" href="/prescriptions/{{$prescription->id}}/edit" role="button">Edit</a>
                            <form role="form" id="prescription_del_form"  onsubmit="return confirm('Do you really want to delete?');" method="POST" action="/prescriptions/{{$prescription->id}}">
                             {{csrf_field()}}
                             {{ method_field('DELETE') }}
                             <button type="submit" class="btn btn-sm btn-danger pull-right">Delete</button>
                           </form>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>            
        </div>
    </div>
</div>

@endsection
