@extends('layouts.app')

@section('content')
<div class="container">
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-6">
            <h3>Update Prescription</h3>
        </div>
        <div class="col-md-6 text-right">
          <a class="btn btn-sm btn-primary pull-right" href="/prescriptions/create" role="button">+ Add New</a>
        </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-12">
        <form role="form" id="prescription_form" method="POST" action="/prescriptions/{{$prescription->id}}">
         {{csrf_field()}}
         {{ method_field('PUT') }}
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="prescription_date">Prescription Date</label>
              <input type="date" class="form-control datepicker" name="prescription_date" id="prescription_date" placeholder="Prescription Date" value="{{$prescription->prescription_date}}" required>
            </div>
            <div class="form-group col-md-6">
              <label for="patient_name">Patient Name</label>
              <input type="text" class="form-control" name="patient_name" id="patient_name" placeholder="Patient Name" value="{{$prescription->patient_name}}" required>
            </div>
          </div>
          <div class="form-group">
            <label for="age">Patient Age</label>
            <input type="number" min="1" max="120" class="form-control" name="age" id="age" placeholder="Age" value="{{$prescription->age}}" required>
          </div>
          <div class="form-group">
            <div class="form-group">
              <label for="gender">Patient Gender</label>
              <select name="gender" id="gender" class="form-control">
                <option  value="" selected>Choose...</option>
                <option value="Male" @if($prescription->gender=='Male') selected @endif>Male</option> 
                <option value="Female" @if($prescription->gender=='Female') selected @endif>Female</option>
              </select>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="diagnosis">Diagnosis</label>
              <textarea class="form-control" name="diagnosis" id="diagnosis">{{$prescription->diagnosis}}</textarea>
            </div>
            <div class="form-group col-md-6">
              <label for="medicines">Medicines</label>
              <textarea class="form-control" name="medicines" id="medicines">{{$prescription->medicines}}</textarea>
            </div>
            <div class="form-group col-md-6">
              <label for="next_visit_date">Next visit date</label>
              <input type="date" class="form-control" name="next_visit_date" id="next_visit_date" placeholder="Age" value="{{$prescription->next_visit_date}}">
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>        
      </div>
    </div>
</div>
@endsection
