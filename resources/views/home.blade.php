@extends('layouts.app')

@section('content')
<div class="container">
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-6">
            <h3>Prescriptions</h3>
        </div>
        <div class="col-md-6 text-right">
            <a class="btn btn-sm btn-primary pull-right" href="/prescriptions/create" role="button">+ Add New</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Email</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>John</td>
                        <td>Doe</td>
                        <td>john@example.com</td>
                      </tr>
                      <tr>
                        <td>Mary</td>
                        <td>Moe</td>
                        <td>mary@example.com</td>
                      </tr>
                      <tr>
                        <td>July</td>
                        <td>Dooley</td>
                        <td>july@example.com</td>
                      </tr>
                    </tbody>
                  </table>            
        </div>
    </div>
</div>

@endsection
