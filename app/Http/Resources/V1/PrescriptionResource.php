<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class PrescriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'prescriptionDate' => $this->prescription_date,
            'patientName' => $this->patient_name,
            'age' => $this->age,
            'gender' => $this->gender,
            'diagnosis' =>$this->diagnosis,
            'medicines' =>$this->medicines,
            'nextVisitDate' =>$this->next_visit_date,
        ];
    }
}
