<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prescription;

class PrescriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $start_date = date('Y-m-01');
        if (!empty($_GET['start_date'])) {
            $start_date = $_GET['start_date'];
        }  
        $end_date = date('Y-m-d');
        if (!empty($_GET['end_date'])) {
            $end_date = $_GET['end_date'];
        }

        $prescriptions = Prescription::whereBetween('prescription_date', [$start_date, $end_date])
                                    ->orderBy('prescription_date','DESC')
                                    ->Get();
        return view('Prescription.index',compact('prescriptions','start_date','end_date'));
    }

    public function create()
    {
        return view('prescription.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'prescription_date' => 'required|date',
            'patient_name' => 'required',
            'age' => 'required|integer|between:1,120',
            'gender' => 'required',
        ]);

        $prescription = Prescription::create($request->all());

        session()->flash('success', 'Successfully saved.');
        return redirect("prescriptions");
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $prescription = Prescription::Find($id);
        if (is_null($prescription)) {
            session()->flash('fail', 'Not Found.');
            return redirect("prescriptions");
        }
        return view('Prescription.edit',compact('prescription'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'prescription_date' => 'required|date',
            'patient_name' => 'required',
            'age' => 'required|integer|between:1,120',
            'gender' => 'required',
        ]);

        $prescription = Prescription::Find($id)->update($request->all());
        if ($prescription) {
            session()->flash('success', 'Successfully updated.');
            return back();
        } else {
            session()->flash('fail', 'Something went wrong!.');
            return back();            
        }
    }

    public function destroy($id)
    {
        $prescription = Prescription::Find($id)->delete();
        if ($prescription) {
            session()->flash('success', 'Successfully deleted.');
            return back();
        } else {
            session()->flash('fail', 'Something went wrong!.');
            return back();            
        }        
    }
    public function dailyPrescriptionCount()
    {
        $prescriptions = Prescription::selectRaw('prescription_date,count(id) as dailyCount')
                                    ->orderBy('prescription_date','DESC')
                                    ->groupBy('prescription_date')->get();
        return view('Prescription.dailyCount',compact('prescriptions'));
    }
}
