<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
	protected $fillable = [
		'prescription_date','patient_name','age','gender','diagnosis','medicines','next_visit_date'
	];
}
